package usu.seal.salad.pattern;

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationInteger;
import com.google.common.collect.BiMap;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import usu.seal.salad.cfg.node.SMethodSignature;

import java.util.*;

/**
 * Created by TamNT on 7/7/15.
 */
public class HAPI {
    //objectID could be objectID or objectSetID
    private int objectID;
    private int nStates;
    private int nMethods;
    private BiMap<Integer, Integer> methodIDBiMap;
    private Hmm<ObservationInteger> hmm;
    private Map<Integer, Double> popularThresholdsByLength;

    public HAPI(int objectID) {
        this.objectID = objectID;
    }

    public double computeProb(List<Integer> currentSequence) {
        List<ObservationInteger> targetSequence = new ArrayList<>();
        for (Integer methodID : currentSequence) {
            targetSequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
        }
        return hmm.probability(targetSequence);
    }

    public boolean evalMethodIDSequence(List<Integer> methodIDSequence) {
        double prob = computeProb(methodIDSequence);
        if (methodIDSequence.size() == 1) {
            return true;
        }
        double threshold = popularThresholdsByLength.get(methodIDSequence.size());
        System.out.println(prob);
        System.out.println(threshold);
        return prob > threshold;
    }

    public Pair<Integer, Integer> findReplaceCandidate(List<Integer> methodIDSequence) {
        List<Triple<Integer, Integer, Double>> candidates = new ArrayList<>();
        List<ObservationInteger> targetSequence = new ArrayList<>();
        for (Integer methodID : methodIDSequence) {
            targetSequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
        }
        candidates.add(Triple.of(-1, -1, hmm.probability(targetSequence)));
        for (int index = 0; index < methodIDSequence.size(); index++) {
            ObservationInteger temp = targetSequence.get(index);
            for (int j = 0; j < nMethods; j++) {
                if (j != temp.value) {
                    targetSequence.set(index, new ObservationInteger(j));
                    candidates.add(Triple.of(methodIDBiMap.inverse().get(j), index, hmm.probability(targetSequence)));
                }
            }
            targetSequence.set(index, temp);
        }

        Collections.sort(candidates, new Comparator<Triple<Integer, Integer, Double>>() {
            @Override
            public int compare(Triple<Integer, Integer, Double> o1, Triple<Integer, Integer, Double> o2) {
                return -Double.compare(o1.getRight(), o2.getRight());
            }
        });
        Triple<Integer, Integer, Double> top = candidates.get(0);
        return Pair.of(top.getLeft(), top.getMiddle());
    }

    public Pair<Integer,Integer> findAddCandidate(List<Integer> methodIDSequence) {
        List<Triple<Integer, Integer, Double>> candidates = new ArrayList<>();
        List<ObservationInteger> targetSequence = new ArrayList<>();
        for (Integer methodID : methodIDSequence) {
            targetSequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
        }
        for (int index = 0; index <= methodIDSequence.size(); index++) {
            targetSequence.add(index, new ObservationInteger(-1));
            for (int j = 0; j < nMethods; j++) {
                targetSequence.set(index, new ObservationInteger(j));
                candidates.add(Triple.of(methodIDBiMap.inverse().get(j), index, hmm.probability(targetSequence)));
            }
            targetSequence.remove(index);
        }
        Collections.sort(candidates, new Comparator<Triple<Integer, Integer, Double>>() {
            @Override
            public int compare(Triple<Integer, Integer, Double> o1, Triple<Integer, Integer, Double> o2) {
                return -Double.compare(o1.getRight(), o2.getRight());
            }
        });
        Triple<Integer, Integer, Double> top = candidates.get(0);
        return Pair.of(top.getLeft(), top.getMiddle());
    }

    public int findDeletePosition(List<Integer> methodIDSequence) {
        List<Pair<Integer, Double>> candidates = new ArrayList<>();
        List<ObservationInteger> targetSequence = new ArrayList<>();
        for (Integer methodID : methodIDSequence) {
            targetSequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
        }
        for (int index = 0; index < methodIDSequence.size(); index++) {
            ObservationInteger temp = targetSequence.get(index);
            targetSequence.remove(index);
            candidates.add(Pair.of(index, hmm.probability(targetSequence)));
            targetSequence.add(index, temp);
        }
        Collections.sort(candidates, new Comparator<Pair<Integer, Double>>() {
            @Override
            public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
                return -Double.compare(o1.getRight(), o2.getRight());
            }
        });
        Pair<Integer, Double> top = candidates.get(0);
        return top.getLeft();
    }

    public List<Pair<Integer, Double>> computeCandidatesForASequence(Pair<List<Integer>, List<Integer>> incompleteSequence) {
        List<Pair<Integer, Double>> rank = new ArrayList<>();
        List<ObservationInteger> predictSequence = new ArrayList<>();
        int methodID;
        for (int i = 0; i < incompleteSequence.getLeft().size(); i++) {
            if (!incompleteSequence.getRight().contains(i)) {
                methodID = incompleteSequence.getLeft().get(i);
                predictSequence.add(new ObservationInteger(methodIDBiMap.get(methodID)));
            } else predictSequence.add(i, new ObservationInteger(-1));
        }
        for (int i = 0; i < nMethods; i++) {
            for (int loc : incompleteSequence.getRight()) {
                predictSequence.set(loc, new ObservationInteger(i));
            }
            rank.add(Pair.of(methodIDBiMap.inverse().get(i), hmm.probability(predictSequence)));
        }
//        Collections.sort(rank, new Comparator<Pair<Integer, Double>>() {
//            @Override
//            public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
//                return -Double.compare(o1.getRight(), o2.getRight());
//            }
//        });
        return rank;
    }

    public int getObjectID() {
        return objectID;
    }

    public void setObjectID(int objectID) {
        this.objectID = objectID;
    }

    public int getnStates() {
        return nStates;
    }

    public void setnStates(int nStates) {
        this.nStates = nStates;
    }

    public int getnMethods() {
        return nMethods;
    }

    public void setnMethods(int nMethods) {
        this.nMethods = nMethods;
    }

    public BiMap<Integer, Integer> getMethodIDBiMap() {
        return methodIDBiMap;
    }

    public void setMethodIDBiMap(BiMap<Integer, Integer> methodIDBiMap) {
        this.methodIDBiMap = methodIDBiMap;
    }

    public Hmm<ObservationInteger> getHmm() {
        return hmm;
    }

    public void setHmm(Hmm<ObservationInteger> hmm) {
        this.hmm = hmm;
    }

    public Map<Integer, Double> getPopularThresholdsByLength() {
        return popularThresholdsByLength;
    }

    public void setPopularThresholdsByLength(Map<Integer, Double> popularThresholdsByLength) {
        this.popularThresholdsByLength = popularThresholdsByLength;
    }


}
