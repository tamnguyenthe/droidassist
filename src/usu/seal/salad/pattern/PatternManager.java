package usu.seal.salad.pattern;

import be.ac.ulg.montefiore.run.jahmm.*;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.opencsv.CSVReader;
import usu.seal.salad.util.Constants;

import java.io.*;
import java.util.*;

/**
 * Created by TamNT on 6/26/15.
 */
public class PatternManager {
    private BiMap<Integer, String> objectBiMap;
    private BiMap<Integer, String> methodNameBiMap;
    private BiMap<Integer, ImmutableList<Integer>> objectSetBiMap;

    public PatternManager() {
        try {
            loadNameMaps();
        } catch (Exception e) {
            System.err.println("Problem when reading name maps");
            e.printStackTrace();
        }
    }

    private void loadNameMaps() throws Exception {
        String[] entries;

        //load the object id map
        objectBiMap = HashBiMap.create();
        InputStream resourceAsStream = this.getClass().getResourceAsStream(Constants.OBJECTS_FILE);

        CSVReader objectsCsvReader = new CSVReader(new InputStreamReader(resourceAsStream), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
        while ((entries = objectsCsvReader.readNext()) != null) {
            objectBiMap.put(Integer.valueOf(entries[1]), entries[0]);
        }
        objectsCsvReader.close();

        //load the method name id map
        methodNameBiMap = HashBiMap.create();
        InputStream resourceAsStream1 = this.getClass().getResourceAsStream(Constants.METHOD_NAMES_FILE);
        CSVReader methodNamesCsvReader = new CSVReader(new InputStreamReader(resourceAsStream1), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
        while ((entries = methodNamesCsvReader.readNext()) != null) {
            methodNameBiMap.put(Integer.valueOf(entries[1]), entries[0]);
        }
        methodNamesCsvReader.close();

        //load the multiple object id map
        ImmutableList<Integer> multipleObjectsIDs;
        objectSetBiMap = HashBiMap.create();
        InputStream resourceAsStream2 = this.getClass().getResourceAsStream(Constants.MULTIPLE_OBJECTS_FILE);
        CSVReader multipleObjectsCsvReader = new CSVReader(new InputStreamReader(resourceAsStream2), Constants.COMMA_CHAR, Constants.QUOTE_CHAR);
        while ((entries = multipleObjectsCsvReader.readNext()) != null) {
            multipleObjectsIDs = getListFromString(entries[0]);
            objectSetBiMap.put(Integer.valueOf(entries[1]), multipleObjectsIDs);
        }
        multipleObjectsCsvReader.close();
    }

    private ImmutableList<Integer> getListFromString(String strArr) {
        String[] parts = strArr.substring(1, strArr.length() - 1).split(", ");
        List<Integer> result = new ArrayList<>(parts.length);
        for (int i = 0; i < parts.length; i++) result.add(Integer.valueOf(parts[i]));
        return ImmutableList.copyOf(result);
    }

    public HAPI loadModelForObject(int objectID) {
        Scanner scanner = null;
        try {
            InputStream resourceAsStream = this.getClass().getResourceAsStream(Constants.SINGLE_OBJECT_USAGE_DIR + "/" + objectID + ".hapi");
            scanner = new Scanner(resourceAsStream);
        } catch (Exception e) {
            System.err.println("Problem when reading model file");
            e.printStackTrace();
        }
        HAPI hapi = new HAPI(objectID);
        int nMethods = scanner.nextInt();
        int nStates = scanner.nextInt();
        hapi.setnMethods(nMethods);
        hapi.setnStates(nStates);
        BiMap<Integer, Integer> methodIDBiMap = HashBiMap.create();
        int key, value;
        for (int i = 0; i < nMethods; i++) {
            key = scanner.nextInt();
            scanner.next();
            value = scanner.nextInt();
            methodIDBiMap.put(key, value);
        }
        hapi.setMethodIDBiMap(methodIDBiMap);
        int numThresholds = scanner.nextInt();
        Map<Integer, Double> popularThresholdsByLength = new HashMap<>();
        int length;
        double threshold;
        for (int i = 0; i < numThresholds; i++) {
            length = scanner.nextInt();
            scanner.next();
            threshold = scanner.nextDouble();
            popularThresholdsByLength.put(length, threshold);
        }
        hapi.setPopularThresholdsByLength(popularThresholdsByLength);
        OpdfIntegerFactory factory = new OpdfIntegerFactory(nMethods);
        Hmm<ObservationInteger> hmm = new Hmm<ObservationInteger>(nStates, factory);
        for (int i = 0; i < nStates; i++) hmm.setPi(i, scanner.nextDouble());
        for (int i = 0; i < nStates; i++) {
            double[] callingDistribution = new double[nMethods];
            for (int j = 0; j < nMethods; j++) {
                callingDistribution[j] = scanner.nextDouble();
            }
            hmm.setOpdf(i, new OpdfInteger(callingDistribution));
        }
        for (int i = 0; i < nStates; i++) {
            for (int j = 0; j < nStates; j++) {
                hmm.setAij(i, j, scanner.nextDouble());
            }
        }
        hapi.setHmm(hmm);
        return hapi;
    }

    public BiMap<Integer, String> getMethodNameBiMap() {
        return methodNameBiMap;
    }

    public BiMap<Integer, String> getObjectBiMap() {
        return objectBiMap;
    }

    public BiMap<Integer, ImmutableList<Integer>> getObjectSetBiMap() {
        return objectSetBiMap;
    }

}
