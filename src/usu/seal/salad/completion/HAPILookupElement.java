package usu.seal.salad.completion;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.completion.util.MethodParenthesesHandler;
import com.intellij.codeInsight.lookup.*;
import com.intellij.codeInsight.lookup.impl.JavaElementLookupRenderer;
import com.intellij.featureStatistics.FeatureUsageTracker;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.editor.Document;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.PsiMethodImpl;
import com.intellij.psi.util.MethodSignature;
import com.intellij.psi.util.PsiFormatUtil;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.ui.RowIcon;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.util.TypeUtil;

import javax.swing.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;

/**
 * Created by TamNT on 7/8/15.
 */
public class HAPILookupElement extends LookupElement {
    private final String type;
    private final SMethodSignature methodSignature;
    private final double likelihood;
    private final Icon icon;

    public HAPILookupElement(String type, SMethodSignature methodSignature, double likelihood) {
        this.type = type;
        this.methodSignature = methodSignature;
        this.likelihood = likelihood;

        RowIcon rowIcon = new RowIcon(2);
        rowIcon.setIcon(AllIcons.Nodes.Method, 0);
        rowIcon.setIcon(type.equals(methodSignature.getContainingClass()) ? AllIcons.Gutter.ImplementingMethod : AllIcons.Gutter.OverridingMethod, 1);
        icon = rowIcon;
    }

    @Override
    public void handleInsert(InsertionContext context) {
        final Document document = context.getDocument();
        final PsiFile file = context.getFile();
        final LookupElement[] allItems = context.getElements();
        final boolean overloadsMatter = allItems.length == 1;
        JavaCompletionUtil.insertParentheses(context, this, overloadsMatter, false);
        context.commitDocument();
    }

    @NotNull
    @Override
    public String getLookupString() {
        return methodSignature.getName();
    }


    @NotNull
    @Override
    public Object getObject() {
        return super.getObject();
    }

    @Override
    public void renderElement(LookupElementPresentation presentation) {
        presentation.setIcon(icon);
        presentation.setItemTextBold(true);
        presentation.setItemText(methodSignature.getName());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        String[] parameters = methodSignature.getParameters();

        if (parameters.length > 0) {
            if (!TypeUtil.isJavaClass(parameters[0])) {
                stringBuilder.append(parameters[0]);
                stringBuilder.append(" ");
                stringBuilder.append(parameters[0].charAt(0));
            } else {
                String className = TypeUtil.getClassNameFromQualifiedName(parameters[0]);
                stringBuilder.append(className);
                stringBuilder.append(" ");
                stringBuilder.append(WordUtils.uncapitalize(className));
            }
            for (int i = 1; i < parameters.length; i++) {
                stringBuilder.append(", ");
                if (!TypeUtil.isJavaClass(parameters[i])) {
                    stringBuilder.append(parameters[i]);
                    stringBuilder.append(" ");
                    stringBuilder.append(parameters[i].charAt(0));
                } else {
                    String className = TypeUtil.getClassNameFromQualifiedName(parameters[i]);
                    stringBuilder.append(className);
                    stringBuilder.append(" ");
                    stringBuilder.append(WordUtils.uncapitalize(className));
                }
            }
        }
        stringBuilder.append("):");
        if (methodSignature.getReturnType() == null)
            System.out.println(methodSignature);
        stringBuilder.append(TypeUtil.getClassNameFromQualifiedName(methodSignature.getReturnType()));
        presentation.setTailText(stringBuilder.toString());
        NumberFormat formatter = new DecimalFormat("#0.00");
        presentation.setTypeText(formatter.format(likelihood*100) + "%");
        super.renderElement(presentation);
    }



    public double getLikelihood() {
        return likelihood;
    }

    public SMethodSignature getMethodSignature() {
        return methodSignature;
    }


}
