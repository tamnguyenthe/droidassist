package usu.seal.salad.completion;

import com.intellij.codeInsight.completion.*;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.PathUtil;
import com.intellij.util.ProcessingContext;
import org.apache.commons.lang3.tuple.Pair;
import usu.seal.salad.cfg.SourceCodeCFGCreator;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.pattern.PatternManager;
import usu.seal.salad.util.TypeUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Created by Tam Nguyen on 6/10/2015.
 */
public class HAPICompletionContributor extends CompletionContributor {
    private static PatternManager patternManager = null;
    public static PatternManager getPatternManager() {
        if (patternManager == null) {
            patternManager = new PatternManager();
        }
        return patternManager;
    }


    public HAPICompletionContributor() throws IOException {
        extend(CompletionType.SMART, PlatformPatterns.psiElement(), new CompletionProvider<CompletionParameters>() {
            @Override
            protected void addCompletions(CompletionParameters parameters, ProcessingContext context, CompletionResultSet result) {
                //get psiMethod that represents the current method
                //if the current completion outside method (e.g. field declaration) just return
                PsiElement psiMethod = PsiTreeUtil.getParentOfType(parameters.getPosition(), PsiMethod.class);
                if (psiMethod == null) return;
                //create a cfg that represents the current method
                SourceCodeCFGCreator cfgCreator = new SourceCodeCFGCreator((PsiMethod)psiMethod, parameters);
                cfgCreator.buildCFG();
                if (cfgCreator.getCfgParameters().getEditingObjectNode() != null) {
                    CompletionQuery query = cfgCreator.createCompletionQuery();
                    HAPIRecommender recommender = new HAPIRecommender(query, HAPICompletionContributor.getPatternManager());
                    List<Pair<SMethodSignature, Double>> candidates = recommender.computeCandidates();
                    for (Pair<SMethodSignature, Double> candidate : candidates) {
                        HAPILookupElement lookupElement = new HAPILookupElement(query.getType(), candidate.getLeft(), candidate.getRight());
                        result.addElement(lookupElement);
                    }
                }
            }
        });
    }


}
