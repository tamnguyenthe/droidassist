package usu.seal.salad.completion;

import com.google.common.collect.ImmutableList;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.highlighter.EditorHighlighter;
import com.intellij.openapi.editor.highlighter.EditorHighlighterFactory;
import com.intellij.openapi.ui.Messages;
import org.apache.commons.lang3.tuple.Pair;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.completion.CompletionQuery;
import usu.seal.salad.pattern.HAPI;
import usu.seal.salad.pattern.PatternManager;
import usu.seal.salad.util.TypeUtil;

import java.util.*;

/**
 * Created by TamNT on 7/7/15.
 */
public class HAPIRecommender {
    private CompletionQuery query;
    private HAPI model;
    private Set<Integer> multipleObjectIDs;
    private PatternManager patternManager;

    public HAPIRecommender(CompletionQuery query, PatternManager patternManager) {
        this.query = query;
        this.patternManager = patternManager;
        int objectID = patternManager.getObjectBiMap().inverse().get(TypeUtil.convertClassStringToDalvik(query.getType()));
        List<Integer> multipleObjects = new ArrayList<>();
        multipleObjectIDs = new HashSet<>();
        for (Set<String> sequence : query.getMultipleObjectsSet()) {
            multipleObjects.clear();
            for (String type: sequence) {
                multipleObjects.add(patternManager.getObjectBiMap().inverse().get(TypeUtil.convertClassStringToDalvik(type)));
            }
            Collections.sort(multipleObjects);
            ImmutableList<Integer> immutableList = ImmutableList.copyOf(multipleObjects);
            Integer integer = patternManager.getObjectSetBiMap().inverse().get(immutableList);
            if (integer != null) {
                multipleObjectIDs.add(integer);
            }
        }
        this.model = patternManager.loadModelForObject(objectID);
    }

    public List<Pair<SMethodSignature, Double>> computeCandidates() {
        List<List<Pair<Integer, Double>>> allRanks = new ArrayList<>();
        for (Pair<List<SMethodSignature>, List<Integer>> methodSequence : query.getMissingMethodSequences()) {
            List<Integer> incompleteSequence = new ArrayList<>();
            int methodID;
            String methodString;
            for (int i = 0; i < methodSequence.getLeft().size(); i++) {
                if (!methodSequence.getRight().contains(i)) {
                    methodString = methodSequence.getLeft().get(i).toDalvikString();
                    System.out.println(methodString);
                    methodID = patternManager.getMethodNameBiMap().inverse().get(methodString);
                    incompleteSequence.add(methodID);
                } else {
                    incompleteSequence.add(-1);
                }
            }
            List<Pair<Integer, Double>> rank = model.computeCandidatesForASequence(Pair.of(incompleteSequence, methodSequence.getRight()));
            allRanks.add(rank);
        }
        List<Pair<Integer, Double>> finalRank = new ArrayList<>();
        double maxProb;
        for (int i = 0; i < allRanks.get(0).size(); i++) {
            Pair<Integer, Double> pair = allRanks.get(0).get(i);
            maxProb = pair.getRight();
            for (List<Pair<Integer, Double>> rank : allRanks) {
                if (maxProb < rank.get(i).getRight()) {
                    maxProb = rank.get(i).getRight();
                }
            }
            finalRank.add(Pair.of(pair.getLeft(), maxProb));
        }
        Collections.sort(finalRank, new Comparator<Pair<Integer, Double>>() {
            @Override
            public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
                return -Double.compare(o1.getRight(), o2.getRight());
            }
        });
        return filterResult(convertIDToMethodSequence(finalRank));
    }

    private List<Pair<SMethodSignature, Double>> filterResult(List<Pair<String, Double>> finalRank) {
        List<Pair<SMethodSignature, Double>> result = new ArrayList<>();
        double sum = 0;
        for (int i = 0; i < finalRank.size(); i++) {
            SMethodSignature methodSignature = TypeUtil.convertMethodDalvikString(finalRank.get(i).getLeft());
            if (methodSignature != null) {
                if (!methodSignature.getContainingClass().equals(methodSignature.getName())) {
                    if (methodSignature.getContainingClass().equals(query.getType())) {
                        result.add(Pair.of(methodSignature, finalRank.get(i).getRight()));
                        sum += finalRank.get(i).getRight();
                    }
                }
            }
        }
        for (int i = 0; i < result.size(); i++) {
            result.set(i, Pair.of(result.get(i).getLeft(), result.get(i).getRight() / sum));
        }
        return result;
    }

    private List<Pair<String, Double>> convertIDToMethodSequence(List<Pair<Integer, Double>> rank) {
        List<Pair<String, Double>> result = new ArrayList<>();
        for (Pair<Integer, Double> pair : rank) {
            String dalvikMethodName = patternManager.getMethodNameBiMap().get(pair.getLeft());
            result.add(Pair.of(dalvikMethodName, pair.getRight()));
        }
        return result;
    }

    public HAPI getModel() {
        return model;
    }

    public void setModel(HAPI model) {
        this.model = model;
    }
}
