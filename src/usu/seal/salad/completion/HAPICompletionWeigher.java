package usu.seal.salad.completion;

import com.intellij.codeInsight.completion.CompletionLocation;
import com.intellij.codeInsight.completion.CompletionWeigher;
import com.intellij.codeInsight.lookup.LookupElement;

/**
 * Created by TamNT on 7/8/15.
 */
public class HAPICompletionWeigher extends CompletionWeigher {
    @Override
    public Comparable weigh(LookupElement element, CompletionLocation location) {
        if (element instanceof HAPILookupElement) {
            HAPILookupElement lookupElement = (HAPILookupElement)element;
            return Double.valueOf(lookupElement.getLikelihood());
        }
        return Double.valueOf(0);
    }
}
