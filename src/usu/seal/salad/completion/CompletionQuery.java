package usu.seal.salad.completion;

import com.intellij.codeInsight.highlighting.HighlightUsagesHandlerFactory;
import org.apache.commons.lang3.tuple.Pair;
import usu.seal.salad.cfg.node.SMethodSignature;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by TamNT on 7/6/15.
 */
public class CompletionQuery {
    private String type;
    private Set<Pair<List<SMethodSignature>, List<Integer>>> missingMethodSequences;
    private Set<Set<String>> multipleObjectsSet;

    public CompletionQuery(String type) {
        this.type = type;
        this.missingMethodSequences = new HashSet<>();
    }

    public void addMethodSequence(Pair<List<SMethodSignature>, List<Integer>> methodSequence) {
        missingMethodSequences.add(methodSequence);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Pair<List<SMethodSignature>, List<Integer>>> getMissingMethodSequences() {
        return missingMethodSequences;
    }

    public Set<Set<String>> getMultipleObjectsSet() {
        return multipleObjectsSet;
    }

    public void setMultipleObjectsSet(Set<Set<String>> multipleObjectsSet) {
        this.multipleObjectsSet = multipleObjectsSet;
    }
}
