package usu.seal.salad.cfg;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.PsiMethodImpl;
import com.intellij.psi.util.PsiMethodUtil;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.util.PsiUtil;
import org.apache.commons.lang3.tuple.Pair;
import usu.seal.salad.cfg.node.*;
import usu.seal.salad.cfg.visitor.SourceCodeCFGVisitor;
import usu.seal.salad.completion.CompletionQuery;
import usu.seal.salad.util.PsiUtils;

import java.util.*;

/**
 * Created by Student on 6/11/2015.
 */
public class SourceCodeCFGCreator {
    private PsiMethod psiMethod; //this is the method under analyzing
    private CompletionParameters completionParameters; //this is for code completion
    private CFGParameters cfgParameters; //this is parameters used to build GROUM

    public SourceCodeCFGCreator(PsiMethod psiMethod, CompletionParameters parameters) {
        this.psiMethod = psiMethod;
        completionParameters = parameters;
        cfgParameters = new CFGParameters(parameters);
    }

    public void buildCFG() {
        initializeCFG();
        createCFG();
    }

    private void initializeCFG() {
        //add fields to variable mapping stack
        cfgParameters.getIdentStack().pushANewMap();
        PsiClass psiClass = PsiTreeUtil.getParentOfType(psiMethod, PsiClass.class);
        PsiField[] allFields = psiClass.getAllFields();
        if (PsiUtils.isStaticMethod(psiMethod)) {
            for (PsiField psiField : allFields) {
                if (!PsiUtils.isStaticField(psiField)) continue;
                if (PsiUtils.isObjectType(psiField.getType())) {
                    SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(),
                                                            psiField.getNameIdentifier().getText(),
                                                            psiField.getType().getCanonicalText());
                    cfgParameters.getCfg().add(objectNode);
                    cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
                }
            }
        } else {
            for (PsiField psiField : allFields) {
                if (PsiUtils.isStaticField(psiField)) continue;
                if (PsiUtils.isObjectType(psiField.getType())) {
                    SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(),
                            psiField.getNameIdentifier().getText(),
                            psiField.getType().getCanonicalText());
                    cfgParameters.getCfg().add(objectNode);
                    cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
                }
            }
        }
        //add parameters to variable mapping stack
        //we treat array type as object type
        cfgParameters.getIdentStack().pushANewMap();
        PsiParameterList psiParameterList = PsiTreeUtil.getChildOfType(psiMethod, PsiParameterList.class);
        if (psiParameterList != null) {
            if (PsiTreeUtil.getChildrenOfType(psiParameterList, PsiParameter.class) != null) {
                for (PsiParameter psiParameter : PsiTreeUtil.getChildrenOfType(psiParameterList, PsiParameter.class)) {
                    if (PsiUtils.isObjectType(psiParameter.getType())) {
                        SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(),
                                psiParameter.getNameIdentifier().getText(),
                                psiParameter.getType().getCanonicalText());
                        cfgParameters.getCfg().add(objectNode);
                        cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
                    }
                }
            }
        }
    }

    private void createCFG() {
        cfgParameters.getIdentStack().pushANewMap();
        PsiCodeBlock psiCodeBlock = PsiTreeUtil.getChildOfType(psiMethod, PsiCodeBlock.class);
        SourceCodeCFGVisitor sourceCodeCFGVisitor = new SourceCodeCFGVisitor(cfgParameters);
        psiCodeBlock.accept(sourceCodeCFGVisitor);
        cfgParameters.getCfg().merge(sourceCodeCFGVisitor.getSubGraph());
        cfgParameters.getCfg().addDataBackEdges();
        for (SNode node : cfgParameters.getCfg().getOuts()) {
            SControlNode controlNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.RETURN_LABEL);
            node.getControlEdges().add(controlNode);
        }
    }

    public CompletionQuery createCompletionQuery() {
        CompletionQuery completionQuery = new CompletionQuery(cfgParameters.getEditingObjectNode().getType());
        Set<SNode> relatedMethodNodes = cfgParameters.getEditingObjectNode().getRelatedDataNodes();
        Set<Set<String>> multipleObjectsSet = new HashSet<>();
        for (SNode relatedMethodNode : relatedMethodNodes) {
            SMethodNode methodNode = (SMethodNode)relatedMethodNode;
            Set<SNode> objectSet = methodNode.getRelatedDataNodes();
            Set<String> multipleObject = new HashSet<>();
            for (SNode node : objectSet) {
                SObjectNode objectNode = (SObjectNode)node;
                multipleObject.add(objectNode.getType());
            }
            if (multipleObject.size() > 1) {
                multipleObjectsSet.add(multipleObject);
            }
        }
        completionQuery.setMultipleObjectsSet(multipleObjectsSet);

        //find start node
        SNode startNode = null;
        for (SNode node : cfgParameters.getCfg().getNodes()) {
            if (!(node instanceof SObjectNode)) {
                startNode = node;
                break;
            }
        }
        CFGState startState = new CFGState(startNode);
        Stack<CFGState> frontier = new Stack<>();
        frontier.add(startState);

        while (!frontier.isEmpty()) {
            CFGState currentState = frontier.pop();
            currentState.getExploredPaths().add(currentState.getStartNode());
            SNode currentNode = currentState.getStartNode();
            while (currentNode.getControlEdges().size() == 1) {
                currentState.getVisitedNodeList().add(currentNode);
                currentNode = (SNode)currentNode.getControlEdges().toArray()[0];
            }
            currentState.getVisitedNodeList().add(currentNode);
            if (currentNode.getControlEdges().size() == 0) {
                List<SMethodSignature> methodSequence = new ArrayList<>();
                List<Integer> positions = new ArrayList<>();
                boolean isCompletePath = true;
                for (SNode node : currentState.getVisitedNodeList()) {
                    System.out.println(node);
                    if (relatedMethodNodes.contains(node)) {
                        SMethodNode methodNode = (SMethodNode)node;
                        methodSequence.add(methodNode.getSignature());
                        if (methodNode.getSignature().equals(SMethodSignature.DUMMY_METHOD_SIGNATURE)) {
                            positions.add(methodSequence.size() - 1);
                            isCompletePath = false;
                        }
                    }
                }
                if (!isCompletePath) {
                    completionQuery.addMethodSequence(Pair.of(methodSequence, positions));
                }
            } else {
                for (SNode nextNode : currentNode.getControlEdges()) {
                    if (!currentState.getExploredPaths().contains(nextNode)) {
                        CFGState nextState = new CFGState(nextNode, currentState.getVisitedNodeList(), currentState.getExploredPaths());
                        frontier.push(nextState);
                    }
                }
            }
        }
        return completionQuery;
    }

    public List<SMethodSignature> getMethodSequenceForEvaluation() {
        List<SMethodSignature> result = new ArrayList<>();
        Set<SNode> relatedMethodNodes = cfgParameters.getEvalObjectNode().getRelatedDataNodes();
        for (SNode relatedMethodNode : relatedMethodNodes) {
            SMethodNode methodNode = (SMethodNode)relatedMethodNode;
            result.add(methodNode.getSignature());
        }
        return result;
    }

    public CompletionParameters getCompletionParameters() {
        return completionParameters;
    }

    public void setCompletionParameters(CompletionParameters completionParameters) {
        this.completionParameters = completionParameters;
    }

    public PsiMethod getPsiMethod() {
        return psiMethod;
    }

    public void setPsiMethod(PsiMethod psiMethod) {
        this.psiMethod = psiMethod;
    }

    public CFGParameters getCfgParameters() {
        return cfgParameters;
    }

    public void setCfgParameters(CFGParameters cfgParameters) {
        this.cfgParameters = cfgParameters;
    }
}
