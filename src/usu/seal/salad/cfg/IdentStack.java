package usu.seal.salad.cfg;

import usu.seal.salad.cfg.node.SObjectNode;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by Tam Nguyen on 6/23/15.
 */
public class IdentStack {
    private Stack<Map<String, SObjectNode>> stack;

    public IdentStack() {
        stack = new Stack<>();
    }

    public void push(Map<String, SObjectNode> identMap) {
        stack.push(identMap);
    }

    public void pushANewMap() {
        stack.push(new HashMap<String, SObjectNode>());
    }

    public void put(String ident, SObjectNode objectNode) {
        stack.peek().put(ident, objectNode);
    }

    public SObjectNode get(String ident) {
        for (int i = stack.size()-1; i >= 0; i--) {
            if (stack.get(i).containsKey(ident))
                return stack.get(i).get(ident);
        }
        return null;
    }

    public boolean contains(String ident) {
        for (int i = stack.size()-1; i >= 0; i--) {
            if (stack.get(i).containsKey(ident))
                return true;
        }
        return false;
    }

    public void pop() {
        stack.pop();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

}
