package usu.seal.salad.cfg;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.psi.PsiIdentifier;
import com.intellij.psi.PsiLocalVariable;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SObjectNode;

import java.util.Map;

/**
 * Created by Tam Nguyen on 6/22/15.
 */
public class CFGParameters {
    private SourceCodeCFG cfg; //this is final control flow graph (GROUM)
    private int numberOfNodes; //number of nodes, used to index nodes
    private IdentStack identStack; //this is a stack to store variable mapping

    //this is for code completion
    private CompletionParameters completionParameters;
    private SObjectNode editingObjectNode; //this is for code completion
    private SMethodNode editingMethodNode; //this is for code completion

    //this is for method sequence evaluation
    private PsiIdentifier varToEval; //this is for method sequence evaluation
    private SObjectNode EvalObjectNode; //this is for method sequence evaluation

    public CFGParameters(CompletionParameters completionParameters){
        cfg = new SourceCodeCFG();
        numberOfNodes = 0;
        identStack = new IdentStack();
        this.completionParameters = completionParameters;
    }

    public int idForNewNode() {
        return numberOfNodes++;
    }

    public int getNumberOfNodes() {
        return numberOfNodes;
    }

    public void setNumberOfNodes(int numberOfNodes) {
        this.numberOfNodes = numberOfNodes;
    }

    public IdentStack getIdentStack() {
        return identStack;
    }

    public void setIdentStack(IdentStack identStack) {
        this.identStack = identStack;
    }

    public SourceCodeCFG getCfg() {
        return cfg;
    }

    public void setCfg(SourceCodeCFG cfg) {
        this.cfg = cfg;
    }

    public CompletionParameters getCompletionParameters() {
        return completionParameters;
    }

    public void setCompletionParameters(CompletionParameters completionParameters) {
        this.completionParameters = completionParameters;
    }

    public SObjectNode getEditingObjectNode() {
        return editingObjectNode;
    }

    public void setEditingObjectNode(SObjectNode editingObjectNode) {
        this.editingObjectNode = editingObjectNode;
    }

    public SMethodNode getEditingMethodNode() {
        return editingMethodNode;
    }

    public void setEditingMethodNode(SMethodNode editingMethodNode) {
        this.editingMethodNode = editingMethodNode;
    }

    public SObjectNode getEvalObjectNode() {
        return EvalObjectNode;
    }

    public void setEvalObjectNode(SObjectNode evalObjectNode) {
        EvalObjectNode = evalObjectNode;
    }

    public PsiIdentifier getVarToEval() {
        return varToEval;
    }

    public void setVarToEval(PsiIdentifier varToEval) {
        this.varToEval = varToEval;
    }
}
