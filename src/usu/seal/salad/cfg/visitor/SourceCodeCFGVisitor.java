package usu.seal.salad.cfg.visitor;

import com.intellij.psi.*;
import com.intellij.psi.impl.source.tree.java.PsiPolyadicExpressionImpl;
import com.intellij.psi.javadoc.*;
import usu.seal.salad.cfg.CFGParameters;
import usu.seal.salad.cfg.SourceCodeCFG;

/**
 * Created by Tam Nguyen on 6/18/2015.
 */
public class SourceCodeCFGVisitor extends JavaElementVisitor {

    protected CFGParameters cfgParameters;

    protected SourceCodeCFG subGraph;

    public SourceCodeCFGVisitor(CFGParameters cfgParameters) {
        this.subGraph = new SourceCodeCFG();
        this.cfgParameters = cfgParameters;
    }

    @Override
    public void visitCodeBlock(PsiCodeBlock block) {
        PsiStatement psiStatements[] = block.getStatements();
        cfgParameters.getIdentStack().pushANewMap();
        for (PsiStatement psiStatement : block.getStatements()) {
            CFGStatementVisitor statementVisitor = new CFGStatementVisitor(cfgParameters);
            psiStatement.accept(statementVisitor);
            subGraph.merge(statementVisitor.getSubGraph());
        }
        cfgParameters.getIdentStack().pop();
    }

    public SourceCodeCFG getSubGraph() {
        return subGraph;
    }

    public void setSubGraph(SourceCodeCFG subGraph) {
        this.subGraph = subGraph;
    }

    public CFGParameters getCfgParameters() {
        return cfgParameters;
    }

    public void setCfgParameters(CFGParameters cfgParameters) {
        this.cfgParameters = cfgParameters;
    }

}