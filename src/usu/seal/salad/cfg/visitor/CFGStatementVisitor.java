package usu.seal.salad.cfg.visitor;

import com.intellij.psi.*;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.usageView.UsageInfo;
import usu.seal.salad.cfg.CFGParameters;
import usu.seal.salad.cfg.SourceCodeCFG;
import usu.seal.salad.cfg.node.SControlNode;
import usu.seal.salad.cfg.node.SMethodNode;
import usu.seal.salad.cfg.node.SNode;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.util.PsiUtils;

/**
 * Created by Tam Nguyen on 6/22/15.
 */
public class CFGStatementVisitor extends SourceCodeCFGVisitor {

    public CFGStatementVisitor(CFGParameters cfgParameters) {
        super(cfgParameters);
    }

    // OK
    @Override
    public void visitLabeledStatement(PsiLabeledStatement statement) {
        PsiStatement insideStatement = statement.getStatement();
        if (insideStatement != null) {
            CFGStatementVisitor statementVisitor = new CFGStatementVisitor(cfgParameters);
            insideStatement.accept(statementVisitor);
            subGraph.merge(statementVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitExpressionStatement(PsiExpressionStatement statement) {
        PsiExpression expression = statement.getExpression();
        CFGExpressionVisitor cfgExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        expression.accept(cfgExpressionVisitor);
        subGraph.merge(cfgExpressionVisitor.getSubGraph());
    }

    // OK
    @Override
    public void visitSynchronizedStatement(PsiSynchronizedStatement statement) {
        // don't process this statement
    }

    //OK
    @Override
    public void visitIfStatement(PsiIfStatement statement) {
        SourceCodeCFG ifSubgraph = new SourceCodeCFG();

        PsiExpression conditionExpression = statement.getCondition();
        if (conditionExpression != null) {
            CFGExpressionVisitor conditionExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            conditionExpression.accept(conditionExpressionVisitor);
            ifSubgraph.merge(conditionExpressionVisitor.getSubGraph());
        }

        SControlNode ifNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.IF_LABEL);
        ifSubgraph.merge(ifNode);

        PsiStatement thenBranch = statement.getThenBranch();
        CFGStatementVisitor thenBranchVisitor = new CFGStatementVisitor(cfgParameters);
        if (thenBranch != null) {
            thenBranch.accept(thenBranchVisitor);
            ifSubgraph.merge(thenBranchVisitor.getSubGraph());
        }

        PsiStatement elseBranch = statement.getElseBranch();
        if (elseBranch != null) {
            ifSubgraph.getOuts().clear();
            ifSubgraph.getOuts().add(ifNode);

            CFGStatementVisitor elseBranchVisitor = new CFGStatementVisitor(cfgParameters);
            elseBranch.accept(elseBranchVisitor);
            ifSubgraph.merge(elseBranchVisitor.getSubGraph());

            // add out nodes of then branch
            ifSubgraph.getOuts().addAll(thenBranchVisitor.getSubGraph().getOuts());
        }

        this.subGraph.merge(ifSubgraph);
    }

    // OK
    @Override
    public void visitBreakStatement(PsiBreakStatement statement) {
        // don't process this statement
    }


    // OK
    @Override
    public void visitReturnStatement(PsiReturnStatement statement) {
        PsiExpression returnExpression = statement.getReturnValue();
        if (returnExpression != null) {
            CFGExpressionVisitor returnExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            returnExpression.accept(returnExpressionVisitor);
            subGraph.merge(returnExpressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitSwitchStatement(PsiSwitchStatement statement) {
        // don't process this statement
    }

    // OK
    @Override
    public void visitThrowStatement(PsiThrowStatement statement) {
        // don't process this statement
    }

    // OK
    @Override
    public void visitSwitchLabelStatement(PsiSwitchLabelStatement statement) {
        // don't process this statement
    }

    // OK
    @Override
    public void visitAssertStatement(PsiAssertStatement statement) {
        // don't process this statement
    }

    // OK
    @Override
    public void visitDoWhileStatement(PsiDoWhileStatement statement) {
        SourceCodeCFG doWhileSubGraph = new SourceCodeCFG();
        //build and merge subgraph of statements in the do statement
        PsiStatement bodyStatement = statement.getBody();
        if (bodyStatement != null) {
            CFGStatementVisitor bodyStatementVisitor = new CFGStatementVisitor(cfgParameters);
            bodyStatement.accept(bodyStatementVisitor);
            doWhileSubGraph.merge(bodyStatementVisitor.getSubGraph());
        }

        //add condition
        PsiExpression conditionExpression = statement.getCondition();
        if (conditionExpression != null) {
            CFGExpressionVisitor conditionExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            conditionExpression.accept(conditionExpressionVisitor);
            doWhileSubGraph.merge(conditionExpressionVisitor.getSubGraph());
        }

        //add while node
        SControlNode whileNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.WHILE_LABEL);
        doWhileSubGraph.merge(whileNode);

        whileNode.getControlEdges().add(doWhileSubGraph.getIn());

        subGraph.merge(doWhileSubGraph);
    }

    // OK
    @Override
    public void visitWhileStatement(PsiWhileStatement statement) {
        SourceCodeCFG whileSubGraph = new SourceCodeCFG();
        //add condition
        PsiExpression conditionExpression = statement.getCondition();
        if (conditionExpression != null) {
            CFGExpressionVisitor conditionExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            conditionExpression.accept(conditionExpressionVisitor);
            whileSubGraph.merge(conditionExpressionVisitor.getSubGraph());
        }

        //add control node
        SControlNode whileNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.WHILE_LABEL);
        whileSubGraph.merge(whileNode);


        PsiStatement whileBodyStatement = statement.getBody();
        CFGStatementVisitor bodyStatementVisitor = new CFGStatementVisitor(cfgParameters);
        if (whileBodyStatement != null) {
            whileBodyStatement.accept(bodyStatementVisitor);
            whileSubGraph.merge(bodyStatementVisitor.getSubGraph());
        }

        for (SNode outNode: bodyStatementVisitor.getSubGraph().getOuts())
            outNode.getControlEdges().add(whileSubGraph.getIn());

        whileSubGraph.getOuts().clear();
        whileSubGraph.getOuts().add(whileNode);

        subGraph.merge(whileSubGraph);
    }

    // OK
    @Override
    public void visitForeachStatement(PsiForeachStatement statement) {
        SourceCodeCFG forEachSubGraph = new SourceCodeCFG();

        cfgParameters.getIdentStack().pushANewMap();

        //process and add iteratedValueExpression
        PsiExpression iteratedValueExpression = statement.getIteratedValue();
        CFGExpressionVisitor iterativeExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        if (iteratedValueExpression != null) {
            iteratedValueExpression.accept(iterativeExpressionVisitor);
            forEachSubGraph.merge(iterativeExpressionVisitor.getSubGraph());
        }


        //add iteration parameter to the identifier map
        PsiParameter iterationParameter = statement.getIterationParameter();
        if (PsiUtils.isObjectType(iterationParameter.getType())) {
            SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(), iterationParameter.getName(), iterationParameter.getType().getCanonicalText());
            forEachSubGraph.add(objectNode);
            cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
            if (iteratedValueExpression  instanceof PsiCallExpression) {
                // just one more careful check
                if (iterativeExpressionVisitor.getSubGraph().getOuts().get(0) instanceof SMethodNode) {
                    SMethodNode lastMethodNode = (SMethodNode) iterativeExpressionVisitor.getSubGraph().getOuts().get(0);
                    lastMethodNode.getDataEdges().clear();
                    lastMethodNode.getDataEdges().add(objectNode);
                }
            }
        }

        //add control node
        SControlNode controlNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.FOREACH_LABEL);
        forEachSubGraph.merge(controlNode);

        //process and add the body of the for loop
        PsiStatement bodyStatement = statement.getBody();
        if (bodyStatement != null) {
            CFGStatementVisitor bodyStatementVisitor = new CFGStatementVisitor(cfgParameters);
            bodyStatement.accept(bodyStatementVisitor);
            forEachSubGraph.merge(bodyStatementVisitor.getSubGraph());
        }

        forEachSubGraph.getOuts().clear();
        forEachSubGraph.getOuts().add(controlNode);

        cfgParameters.getIdentStack().pop();

        subGraph.merge(forEachSubGraph);
    }

    // OK
    @Override
    public void visitForStatement(PsiForStatement statement) {
        SourceCodeCFG forSubGraph = new SourceCodeCFG();

        cfgParameters.getIdentStack().pushANewMap();
        //process the initialization statement
        PsiStatement initializationStatement = statement.getInitialization();
        if (initializationStatement != null) {
            CFGStatementVisitor initializationStatementVisitor = new CFGStatementVisitor(cfgParameters);
            initializationStatement.accept(initializationStatementVisitor);
            forSubGraph.merge(initializationStatementVisitor.getSubGraph());
        }

        //process the condition
        PsiExpression conditionExpression = statement.getCondition();
        CFGExpressionVisitor conditionExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        if (conditionExpression != null) {
            conditionExpression.accept(conditionExpressionVisitor);
            forSubGraph.merge(conditionExpressionVisitor.getSubGraph());
        }

        SControlNode controlNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.FOR_LABEL);
        forSubGraph.merge(controlNode);

        //process the body of the for loop
        PsiStatement bodyStatement = statement.getBody();
        if (bodyStatement != null) {
            CFGStatementVisitor bodyStatementVisitor = new CFGStatementVisitor(cfgParameters);
            bodyStatement.accept(bodyStatementVisitor);
            forSubGraph.merge(bodyStatementVisitor.getSubGraph());
        }

        //process update statement
        PsiStatement updateStatement = statement.getUpdate();
        CFGStatementVisitor updateStatementVisitor = new CFGStatementVisitor(cfgParameters);
        if (updateStatement != null) {
            updateStatement.accept(updateStatementVisitor);
            forSubGraph.merge(updateStatementVisitor.getSubGraph());
        }

        // add control edge back from update statement to conditional statement or control node
        SNode conditionInNode = conditionExpressionVisitor.getSubGraph().getIn();
        if (conditionInNode != null) {
            for (SNode outNode : updateStatementVisitor.getSubGraph().getNodes()) outNode.getControlEdges().add(conditionInNode);
        } else {
            for (SNode outNode : updateStatementVisitor.getSubGraph().getNodes()) outNode.getControlEdges().add(controlNode);
        }

        forSubGraph.getOuts().clear();
        forSubGraph.getOuts().add(controlNode);

        cfgParameters.getIdentStack().pop();

        subGraph.merge(forSubGraph);
    }

    // OK
    @Override
    public void visitContinueStatement(PsiContinueStatement statement) {
        // don't process this statement
    }

    // OK
    @Override
    public void visitExpressionListStatement(PsiExpressionListStatement statement) {
        PsiExpression[] expressionList = statement.getExpressionList().getExpressions();
        for (PsiExpression expression: expressionList) {
            CFGExpressionVisitor expressionVisitor = new CFGExpressionVisitor(cfgParameters);
            expression.accept(expressionVisitor);
            subGraph.merge(expressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitTryStatement(PsiTryStatement statement) {
        PsiCodeBlock tryBlock = statement.getTryBlock();
        if (tryBlock != null) {
            SourceCodeCFGVisitor tryBlockVisitor = new SourceCodeCFGVisitor(cfgParameters);
            tryBlock.accept(tryBlockVisitor);
            subGraph.merge(tryBlockVisitor.getSubGraph());
        }
        // don't consider catch blocks
        PsiCodeBlock finallyBlock = statement.getFinallyBlock();
        if (finallyBlock != null) {
            SourceCodeCFGVisitor finallyBlockVisitor = new SourceCodeCFGVisitor(cfgParameters);
            finallyBlock.accept(finallyBlockVisitor);
            subGraph.merge(finallyBlockVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitEmptyStatement(PsiEmptyStatement statement) {
        //don't process this statement
    }

    // OK
    @Override
    public void visitDeclarationStatement(PsiDeclarationStatement statement) {
        for (PsiElement localVariable: statement.getDeclaredElements()) {
            PsiLocalVariable psiLocalVariable = (PsiLocalVariable)localVariable;
            if (PsiUtils.isObjectType(psiLocalVariable.getType())) {
                SObjectNode objectNode = new SObjectNode(cfgParameters.idForNewNode(), psiLocalVariable.getNameIdentifier().getText(), psiLocalVariable.getType().getCanonicalText());
                subGraph.add(objectNode);
                cfgParameters.getIdentStack().put(objectNode.getIdentifier(), objectNode);
                if (psiLocalVariable.hasInitializer()) {
                    PsiExpression psiExpression = psiLocalVariable.getInitializer();
                    CFGExpressionVisitor cfgExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                    psiExpression.accept(cfgExpressionVisitor);
                    subGraph.merge(cfgExpressionVisitor.getSubGraph());
                    if (psiExpression instanceof PsiCallExpression) {
                        // just one more careful check
                        if (cfgExpressionVisitor.getSubGraph().getOuts().get(0) instanceof SMethodNode) {
                            SMethodNode lastMethodNode = (SMethodNode) cfgExpressionVisitor.getSubGraph().getOuts().get(0);
                            lastMethodNode.getDataEdges().clear();
                            lastMethodNode.getDataEdges().add(objectNode);
                        }
                    }
                }
            } else if (psiLocalVariable.hasInitializer()) {
                PsiExpression psiExpression = psiLocalVariable.getInitializer();
                CFGExpressionVisitor cfgExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                psiExpression.accept(cfgExpressionVisitor);
                subGraph.merge(cfgExpressionVisitor.getSubGraph());
            }
        }
    }

    // OK
    @Override
    public void visitBlockStatement(PsiBlockStatement statement) {
        PsiCodeBlock codeBlock = statement.getCodeBlock();
        SourceCodeCFGVisitor codeBlockVisitor = new SourceCodeCFGVisitor(cfgParameters);
        codeBlock.accept(codeBlockVisitor);
        subGraph.merge(codeBlockVisitor.getSubGraph());
    }
}
