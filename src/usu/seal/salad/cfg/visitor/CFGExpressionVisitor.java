package usu.seal.salad.cfg.visitor;

import com.intellij.codeInsight.completion.CompletionInitializationContext;
import com.intellij.codeInsight.completion.CompletionUtilCore;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import usu.seal.salad.cfg.CFGParameters;
import usu.seal.salad.cfg.SourceCodeCFG;
import usu.seal.salad.cfg.node.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Student on 6/18/2015.
 */
public class CFGExpressionVisitor extends SourceCodeCFGVisitor {
    private SObjectNode outObjectNode;
    public CFGExpressionVisitor(CFGParameters cfgParameters) {
        super(cfgParameters);
    }

    // OK
    @Override
    public void visitPolyadicExpression(PsiPolyadicExpression expression) {
        PsiExpression[] operandExpressions = expression.getOperands();
        for (PsiExpression operandExpression : operandExpressions) {
            if (operandExpression != null) {
                CFGExpressionVisitor operandExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                operandExpression.accept(operandExpressionVisitor);
                subGraph.merge(operandExpressionVisitor.getSubGraph());
            }
        }
    }

    // OK
    @Override
    public void visitBinaryExpression(PsiBinaryExpression expression) {
        PsiExpression lExpression = expression.getLOperand();
        CFGExpressionVisitor lExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        lExpression.accept(lExpressionVisitor);
        subGraph.merge(lExpressionVisitor.getSubGraph());

        PsiExpression rExpression = expression.getROperand();
        if (rExpression != null) {
            CFGExpressionVisitor rExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            rExpression.accept(rExpressionVisitor);
            subGraph.merge(rExpressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitPostfixExpression(PsiPostfixExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        CFGExpressionVisitor operandExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        operandExpression.accept(operandExpressionVisitor);
        subGraph.merge(operandExpressionVisitor.getSubGraph());
    }

    // TODO
    @Override
    public void visitReferenceExpression(PsiReferenceExpression expression) {
        //TODO: need to improve the implementation
        //TODO: need to handle the case for instance variable, static method, etc.
        //at this moment, only process the case which it has a PsiIdentifier as a child
        PsiElement psiElement = expression.getLastChild();
        if (psiElement instanceof PsiIdentifier) {
            PsiIdentifier lastChild = (PsiIdentifier) psiElement;
            if (lastChild.getText().equals(CompletionUtilCore.DUMMY_IDENTIFIER_TRIMMED)) {
                SMethodNode methodNode = new SMethodNode(cfgParameters.idForNewNode(), SMethodSignature.DUMMY_METHOD_SIGNATURE);
                PsiExpression prevExpression = PsiTreeUtil.getPrevSiblingOfType(lastChild, PsiExpression.class);
                CFGExpressionVisitor prevExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                prevExpression.accept(prevExpressionVisitor);
                subGraph.merge(prevExpressionVisitor.getSubGraph());
                SObjectNode currentObjectNode = prevExpressionVisitor.getOutObjectNode();
                if (currentObjectNode != null) {
                    currentObjectNode.getDataEdges().add(methodNode);
                    subGraph.merge(methodNode);
                    cfgParameters.setEditingObjectNode(currentObjectNode);
                    cfgParameters.setEditingMethodNode(methodNode);
                }
            } else {
                outObjectNode = cfgParameters.getIdentStack().get(lastChild.getText());

                if ((outObjectNode != null) && (cfgParameters.getVarToEval() != null) && outObjectNode.getIdentifier().equals(cfgParameters.getVarToEval().getText()))
                    cfgParameters.setEvalObjectNode(outObjectNode);
            }
        }
    }

    // OK
    @Override
    public void visitArrayAccessExpression(PsiArrayAccessExpression expression) {
        PsiExpression indexExpression = expression.getIndexExpression();
        if (indexExpression != null) {
            CFGExpressionVisitor indexExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            indexExpression.accept(indexExpressionVisitor);
            subGraph.merge(indexExpressionVisitor.getSubGraph());
        }

        PsiExpression arrayExpression = expression.getArrayExpression();
        CFGExpressionVisitor arrayExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        arrayExpression.accept(arrayExpressionVisitor);
        subGraph.merge(arrayExpressionVisitor.getSubGraph());
    }

    // OK
    @Override
    public void visitMethodCallExpression(PsiMethodCallExpression expression) {

        SMethodNode methodNode = null;
        //create a method node that represents the method in the expression
        if (expression.resolveMethod() != null) {
            methodNode = new SMethodNode(cfgParameters.idForNewNode(), new SMethodSignature(expression.resolveMethod(), expression));

            //process the caller side of the method

            PsiExpression qualifierExpression = expression.getMethodExpression().getQualifierExpression();
            CFGExpressionVisitor callerCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            if (qualifierExpression != null) {
                qualifierExpression.accept(callerCFGExpressionVisitor);
                subGraph.merge(callerCFGExpressionVisitor.getSubGraph());
            }
            SObjectNode callerOutObjectNode = callerCFGExpressionVisitor.getOutObjectNode();
            if (callerOutObjectNode != null) { //exclude primitive types and strings
                callerOutObjectNode.getDataEdges().add(methodNode);
            }


            //process parameters of the method
            PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
            for (PsiExpression parameterExpression : parameterExpressions) {
                //for each expression represents an parameters, merge subgraph
                CFGExpressionVisitor paramCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                parameterExpression.accept(paramCFGExpressionVisitor);
                subGraph.merge(paramCFGExpressionVisitor.getSubGraph());
                //add data edges from nodes represent parameters to the method node
                SObjectNode paramOutObjectNode = paramCFGExpressionVisitor.getOutObjectNode();
                if (paramOutObjectNode != null) {
                    paramOutObjectNode.getDataEdges().add(methodNode);
                }
            }

            //add the method node to the subgraph
            for (SNode outNode : subGraph.getOuts())
                outNode.getControlEdges().add(methodNode);
            subGraph.add(methodNode);
            if (subGraph.getIn() == null) subGraph.setIn(methodNode);
            subGraph.getOuts().clear(); //for defending
            subGraph.getOuts().add(methodNode);

            //create object node represents return values
            PsiType returnType = expression.resolveMethod().getReturnType();
            if (!(returnType instanceof PsiPrimitiveType)) {
                outObjectNode = cfgParameters.getIdentStack().get(expression.getText());
                if (outObjectNode == null) {
                    outObjectNode = new SObjectNode(cfgParameters.idForNewNode(), expression.getText(), returnType.getCanonicalText());
                    subGraph.add(outObjectNode);
                    cfgParameters.getIdentStack().put(expression.getText(), outObjectNode);
                }
                methodNode.getDataEdges().add(outObjectNode);
            }
        } else {
            //process the caller side of the method
            CFGExpressionVisitor callerCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            expression.getMethodExpression().getQualifierExpression().accept(callerCFGExpressionVisitor);
            subGraph.merge(callerCFGExpressionVisitor.getSubGraph());

            //process parameters of the method
            PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
            for (PsiExpression parameterExpression : parameterExpressions) {
                //for each expression represents an parameters, merge subgraph
                CFGExpressionVisitor paramCFGExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                parameterExpression.accept(paramCFGExpressionVisitor);
                subGraph.merge(paramCFGExpressionVisitor.getSubGraph());
            }
        }

    }


    // OK
    @Override
    public void visitNewExpression(PsiNewExpression expression) {
        //create a method node represents the constructor
        //and correct the return type because contructors return null
        if (expression.resolveMethod() != null) {
            SMethodNode methodNode = new SMethodNode(cfgParameters.idForNewNode(), new SMethodSignature(expression.resolveMethod(), expression));
            methodNode.getSignature().setReturnType(methodNode.getSignature().getContainingClass());
            //process the parameters of the method
            PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
            for (PsiExpression parameterExpression: parameterExpressions) {
                CFGExpressionVisitor paramExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                parameterExpression.accept(paramExpressionVisitor);
                subGraph.merge(paramExpressionVisitor.getSubGraph());
                //add data edges from nodes represent parameters to the method node
                SObjectNode paramOutObjectNode = paramExpressionVisitor.getOutObjectNode();
                if (paramOutObjectNode != null) {
                    paramOutObjectNode.getDataEdges().add(methodNode);
                }
            }

            //add the method node to the subgraph
            for (SNode outNode: subGraph.getOuts())
                outNode.getControlEdges().add(methodNode);
            subGraph.add(methodNode);
            if (subGraph.getIn() == null) subGraph.setIn(methodNode);
            subGraph.getOuts().clear(); //just for defending
            subGraph.getOuts().add(methodNode);

            //create an object node represents return values
            PsiType returnType = expression.getType();
            if (!(returnType instanceof PsiPrimitiveType)) {
                outObjectNode = cfgParameters.getIdentStack().get(expression.getText());
                if (outObjectNode == null) {
                    outObjectNode = new SObjectNode(cfgParameters.idForNewNode(), expression.getText(), returnType.getCanonicalText());
                    subGraph.add(outObjectNode);
                    cfgParameters.getIdentStack().put(expression.getText(), outObjectNode);
                }
                methodNode.getDataEdges().add(outObjectNode);
            }
        } else {
            //process the parameters of the method
            if (expression.getArgumentList() != null) {
                PsiExpression[] parameterExpressions = expression.getArgumentList().getExpressions();
                for (PsiExpression parameterExpression: parameterExpressions) {
                    if (parameterExpression != null) {
                        CFGExpressionVisitor paramExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                        parameterExpression.accept(paramExpressionVisitor);
                        subGraph.merge(paramExpressionVisitor.getSubGraph());
                    }
                }
            }
        }

    }


    // OK
    @Override
    public void visitArrayInitializerExpression(PsiArrayInitializerExpression expression) {
        PsiExpression[] initializerExpressions = expression.getInitializers();
        for (PsiExpression initializerExpression : initializerExpressions) {
            if (initializerExpression != null) {
                CFGExpressionVisitor initializerExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
                initializerExpression.accept(initializerExpressionVisitor);
                subGraph.merge(initializerExpressionVisitor.getSubGraph());
            }
        }
    }

    // OK
    @Override
    public void visitSuperExpression(PsiSuperExpression expression) {
        //don't process this expression
    }

    // OK
    @Override
    public void visitThisExpression(PsiThisExpression expression) {
        //don't process this expression
    }

    // OK
    @Override
    public void visitInstanceOfExpression(PsiInstanceOfExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        CFGExpressionVisitor operandExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        operandExpression.accept(operandExpressionVisitor);
        subGraph.merge(operandExpressionVisitor.getSubGraph());
    }

    // OK
    @Override
    public void visitLiteralExpression(PsiLiteralExpression expression) {
        //don't process this expression
    }

    // OK
    @Override
    public void visitAssignmentExpression(PsiAssignmentExpression expression) {
        PsiExpression rExpression = expression.getRExpression();
        CFGExpressionVisitor rCfgExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        if (rExpression != null) {
            rExpression.accept(rCfgExpressionVisitor);
            subGraph.merge(rCfgExpressionVisitor.getSubGraph());

        }

        PsiExpression lExpression = expression.getLExpression();
        CFGExpressionVisitor lCfgExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        lExpression.accept(lCfgExpressionVisitor);

        if (rCfgExpressionVisitor.getOutObjectNode() != null) {
            if (rExpression instanceof PsiCallExpression) {
                // just one more careful check
                if (rCfgExpressionVisitor.getSubGraph().getOuts().get(0) != null) {
                    SMethodNode lastMethodNode = (SMethodNode) rCfgExpressionVisitor.getSubGraph().getOuts().get(0);
                    lastMethodNode.getDataEdges().clear();
                    lastMethodNode.getDataEdges().add(lCfgExpressionVisitor.getOutObjectNode());
                }
            }
        }

    }

    // OK
    @Override
    public void visitLambdaExpression(PsiLambdaExpression expression) {
        // don't process this expression
    }

    // OK
    @Override
    public void visitMethodReferenceExpression(PsiMethodReferenceExpression expression) {
        // don't process this expression
    }

    // OK
    @Override
    public void visitClassObjectAccessExpression(PsiClassObjectAccessExpression expression) {
        // don't process this expression
    }

    // OK
    @Override
    public void visitConditionalExpression(PsiConditionalExpression expression) {
        //this is: cond ? expr true : expr false
        SourceCodeCFG conditionalSubGraph = new SourceCodeCFG();

        PsiExpression conditionalExpression = expression.getCondition();
        CFGExpressionVisitor conditionalExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        conditionalExpression.accept(conditionalExpressionVisitor);
        conditionalSubGraph.merge(conditionalExpressionVisitor.getSubGraph());

        SControlNode controlNode = new SControlNode(cfgParameters.idForNewNode(), SControlNode.IF_LABEL);
        conditionalSubGraph.merge(controlNode);

        //the then expression is @Nullable
        PsiExpression thenExpression = expression.getThenExpression();
        CFGExpressionVisitor thenExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
        if (thenExpression != null) {
            thenExpression.accept(thenExpressionVisitor);
            conditionalSubGraph.merge(thenExpressionVisitor.getSubGraph());
        }

        //the else expression is @Nullable
        PsiExpression elseExpression = expression.getElseExpression();
        if (elseExpression != null) {
            CFGExpressionVisitor elseExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            elseExpression.accept(elseExpressionVisitor);
            conditionalSubGraph.getOuts().clear();
            conditionalSubGraph.getOuts().add(controlNode);
            conditionalSubGraph.merge(elseExpressionVisitor.getSubGraph());

            conditionalSubGraph.getOuts().addAll(thenExpressionVisitor.getSubGraph().getNodes());
        }

        subGraph.merge(conditionalSubGraph);
    }

    // OK
    @Override
    public void visitParenthesizedExpression(PsiParenthesizedExpression expression) {
        PsiExpression insideExpression = expression.getExpression();
        if (insideExpression != null) {
            CFGExpressionVisitor insideExpressionVisitor = new CFGExpressionVisitor(cfgParameters);
            insideExpression.accept(insideExpressionVisitor);
            subGraph.merge(insideExpressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitPrefixExpression(PsiPrefixExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        if (operandExpression != null) {
            CFGExpressionVisitor expressionVisitor = new CFGExpressionVisitor(cfgParameters);
            operandExpression.accept(expressionVisitor);
            subGraph.merge(expressionVisitor.getSubGraph());
        }
    }

    // OK
    @Override
    public void visitTypeCastExpression(PsiTypeCastExpression expression) {
        PsiExpression operandExpression = expression.getOperand();
        if (operandExpression != null) {
            CFGExpressionVisitor expressionVisitor = new CFGExpressionVisitor(cfgParameters);
            operandExpression.accept(expressionVisitor);
            subGraph.merge(expressionVisitor.getSubGraph());
        }
    }

    public SObjectNode getOutObjectNode() {
        return outObjectNode;
    }

    public void setOutObjectNode(SObjectNode outObjectNode) {
        this.outObjectNode = outObjectNode;
    }
}
