package usu.seal.salad.cfg.node;

/**
 * Created by Tam Nguyen on 6/11/2015.
 */
public class SControlNode extends SNode {
    public static final String WHILE_LABEL = "WHILE";
    public static final String IF_LABEL = "IF";
    public static final String FOR_LABEL = "FOR";
    public static final String FOREACH_LABEL = "FOREACH";
    public static final String RETURN_LABEL = "RETURN";

    private String label;

    public SControlNode(int id, String label) {
        super(id);
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "SControlNode{" +
                "id=" + id +
                ", label='" + label + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SControlNode)) return false;
        SControlNode controlNode = (SControlNode)obj;
        return this.getId() == controlNode.getId() &&
                this.getLabel().equals(controlNode.getLabel());
    }
}
