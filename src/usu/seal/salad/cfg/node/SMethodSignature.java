package usu.seal.salad.cfg.node;

import com.intellij.codeInsight.completion.CompletionUtilCore;
import com.intellij.psi.PsiCall;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiParameter;
import com.intellij.psi.PsiType;
import usu.seal.salad.util.PrimitiveType;
import usu.seal.salad.util.TypeUtil;

import java.util.Arrays;

/**
 * Created by Tam Nguyen on 6/19/15.
 */
public class SMethodSignature {
    private String containingClass;
    private String name;
    private String[] parameters;
    private String returnType;
    private PsiCall psiCall; //this is for locate the position of corresponding method call
    private boolean isConstrutor; //this is to indicate whether the method is constructor or not

    public static SMethodSignature DUMMY_METHOD_SIGNATURE = new SMethodSignature(); //this is a dummy method used to temporary fill in missing location

    public SMethodSignature(PsiMethod psiMethod, PsiCall psiCall) {
        isConstrutor = psiMethod.isConstructor();
        this.psiCall = psiCall;
        containingClass = psiMethod.getContainingClass().getQualifiedName();
        name = psiMethod.getName();
        PsiParameter[] psiParameters = psiMethod.getParameterList().getParameters();
        parameters = new String[psiParameters.length];
        for (int i = 0; i < psiParameters.length; i++)
            parameters[i] = psiParameters[i].getType().getCanonicalText();
        returnType = psiMethod.getReturnType() == null ? null : psiMethod.getReturnType().getCanonicalText();
    }

    public SMethodSignature() {
        containingClass = PsiType.VOID.getPresentableText();
        name = CompletionUtilCore.DUMMY_IDENTIFIER_TRIMMED;
        parameters = new String[0];
        returnType = PsiType.VOID.getPresentableText();
        isConstrutor = false;
    }


    @Override
    public String toString() {
        return "SMethodSignature{" +
                "containingClass='" + containingClass + '\'' +
                ", name='" + name + '\'' +
                ", parameters=" + Arrays.toString(parameters) +
                ", returnType='" + returnType + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = result * 31 + containingClass.hashCode();
        result = result * 31 + name.hashCode();
        result = result * 31 + Arrays.hashCode(parameters);
        result = result * 31 + returnType.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SMethodSignature)) return false;
        SMethodSignature methodSignature = (SMethodSignature) obj;
        return this.containingClass.equals(methodSignature.getContainingClass()) &&
                this.name.equals(methodSignature.getName()) &&
                Arrays.equals(this.parameters, methodSignature.getParameters()) &&
                this.returnType.equals(methodSignature.getReturnType());
    }

    public String toDalvikString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(TypeUtil.convertClassStringToDalvik(containingClass));
        if (isConstrutor) stringBuilder.append("-><init>(");
        else stringBuilder.append("->" + name +"(");
        for (String param: parameters) {
            if (PrimitiveType.getPrimitiveMap().containsKey(param))
                stringBuilder.append(PrimitiveType.getPrimitiveMap().get(param));
            else stringBuilder.append(TypeUtil.convertClassStringToDalvik(param));
        }
        stringBuilder.append(")");
        if (isConstrutor) stringBuilder.append(PrimitiveType.VOID);
        else {
            if (PrimitiveType.getPrimitiveMap().containsKey(returnType))
                stringBuilder.append(PrimitiveType.getPrimitiveMap().get(returnType));
            else stringBuilder.append(TypeUtil.convertClassStringToDalvik(returnType));
        }
        return stringBuilder.toString();
    }

    public String getContainingClass() {
        return containingClass;
    }

    public void setContainingClass(String containingClass) {
        this.containingClass = containingClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getParameters() {
        return parameters;
    }

    public void setParameters(String[] parameters) {
        this.parameters = parameters;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public PsiCall getPsiCall() {
        return psiCall;
    }

    public void setPsiCall(PsiCall psiCall) {
        this.psiCall = psiCall;
    }

}
