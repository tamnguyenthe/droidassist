package usu.seal.salad.cfg.node;

/**
 * Created by Tam Nguyen on 6/11/2015.
 */
public class SMethodNode extends SNode implements Comparable<SMethodNode> {

    private SMethodSignature signature;

    public SMethodNode(int id, SMethodSignature signature) {
        super(id);
        this.signature = signature;
    }

    public SMethodSignature getSignature() {
        return signature;
    }

    public void setSignature(SMethodSignature signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "SMethodNode{" +
                "id=" + id +
                ", signature=" + signature +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SMethodNode)) return false;
        SMethodNode methodNode = (SMethodNode)obj;
        return this.getId() == methodNode.getId() &&
                this.getSignature().equals(methodNode.getSignature());
    }

    @Override
    public int compareTo(SMethodNode o) {
        return Integer.compare(getId(), o.getId());
    }
}

