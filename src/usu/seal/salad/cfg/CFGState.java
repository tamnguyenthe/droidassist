package usu.seal.salad.cfg;

import usu.seal.salad.cfg.node.SNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by TamNT on 7/6/15.
 */
public class CFGState {
    private SNode startNode;
    private List<SNode> visitedNodeList;
    private Set<SNode> exploredPaths;

    public CFGState(SNode startNode) {
        this.startNode = startNode;
        visitedNodeList = new ArrayList<>();
        exploredPaths = new HashSet<>();
    }

    public CFGState(SNode startNode, List<SNode> visitedNodeList, Set<SNode> exploredPaths) {
        this.startNode = startNode;
        this.visitedNodeList = new ArrayList<>();
        for (SNode node : visitedNodeList) this.visitedNodeList.add(node);
        this.exploredPaths = new HashSet<>();
        for (SNode node : exploredPaths) this.exploredPaths.add(node);
    }

    public SNode getStartNode() {
        return startNode;
    }

    public void setStartNode(SNode startNode) {
        this.startNode = startNode;
    }

    public List<SNode> getVisitedNodeList() {
        return visitedNodeList;
    }

    public void setVisitedNodeList(List<SNode> visitedNodeList) {
        this.visitedNodeList = visitedNodeList;
    }

    public Set<SNode> getExploredPaths() {
        return exploredPaths;
    }

    public void setExploredPaths(Set<SNode> exploredPaths) {
        this.exploredPaths = exploredPaths;
    }
}
