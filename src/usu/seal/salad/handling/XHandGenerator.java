package usu.seal.salad.handling;

public class XHandGenerator {

    private String generatedCode;

    public XHandGenerator(String generatedCode) {
        this.generatedCode = generatedCode;
    }

    public String getTemplateDescription() {
        return generatedCode;
    }
}
