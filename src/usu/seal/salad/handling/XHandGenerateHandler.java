package usu.seal.salad.handling;

import com.intellij.codeInsight.CodeInsightActionHandler;
import com.intellij.codeInsight.hint.HintManager;
import com.intellij.ide.DataManager;
import com.intellij.lang.surroundWith.Surrounder;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorModificationUtil;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.psi.PsiCompiledElement;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.util.ui.UIUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class XHandGenerateHandler implements CodeInsightActionHandler {

    private static final String CHOOSER_TITLE = "Handling Sequences";

    @Override
    public boolean startInWriteAction() {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, @NotNull Editor editor, @NotNull PsiFile file) {
        if (!EditorModificationUtil.checkModificationAllowed(editor)) return;
        if (file instanceof PsiCompiledElement) {
            HintManager.getInstance().showErrorHint(editor, "Can't modify decompiled code");
            return;
        }

        List<AnAction> applicable = buildXHandGeneratorActions(project, editor, file);
        if (applicable != null) {
            showPopup(editor, applicable);
        }
        else if (!ApplicationManager.getApplication().isUnitTestMode()) {
            HintManager.getInstance().showErrorHint(editor, "Couldn't find Surround With variants applicable to the current context");
        }
    }

    private List<AnAction> buildXHandGeneratorActions(Project project, Editor editor, PsiFile file) {
        XHandGeneratorAction action = new XHandGeneratorAction(new XHandGenerator("bufferredReader.clode();"), project, editor, '1');
        List<AnAction> applicable = new ArrayList<>();
        applicable.add(action);
        return applicable;
    }

    private static void showPopup(Editor editor, List<AnAction> applicable) {
        DataContext context = DataManager.getInstance().getDataContext(editor.getContentComponent());
        JBPopupFactory.ActionSelectionAid mnemonics = JBPopupFactory.ActionSelectionAid.MNEMONICS;
        DefaultActionGroup group = new DefaultActionGroup(applicable.toArray(new AnAction[applicable.size()]));
        JBPopupFactory.getInstance().createActionGroupPopup(CHOOSER_TITLE, group, context, mnemonics, true).showInBestPositionFor(editor);
    }

    static void generateHandlingSequences(Project myProject, Editor myEditor, XHandGenerator myGenerator) {

    }

    private static class XHandGeneratorAction extends AnAction {
        private final XHandGenerator myGenerator;
        private final Project myProject;
        private final Editor myEditor;

        public XHandGeneratorAction(XHandGenerator generator, Project project, Editor editor, char mnemonic) {
            super(UIUtil.MNEMONIC + String.valueOf(mnemonic) + ". " + generator.getTemplateDescription());
            myGenerator = generator;
            myProject = project;
            myEditor = editor;
        }

        @Override
        public void actionPerformed(AnActionEvent e) {
            if (!FileDocumentManager.getInstance().requestWriting(myEditor.getDocument(), myProject)) {
                return;
            }

            WriteCommandAction.runWriteCommandAction(myProject, () -> generateHandlingSequences(myProject, myEditor, myGenerator));
        }
    }
}
