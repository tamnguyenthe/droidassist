package usu.seal.salad.handling;

import com.intellij.codeInsight.CodeInsightActionHandler;
import com.intellij.codeInsight.actions.BaseCodeInsightAction;
import com.intellij.codeInsight.template.impl.TemplateManagerImpl;
import com.intellij.lang.Language;
import com.intellij.lang.LanguageSurrounders;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiUtilCore;
import org.jetbrains.annotations.NotNull;
import usu.seal.salad.exception.XRankSurroundWithHandler;

public class XHandAction extends BaseCodeInsightAction {

    public XHandAction() {
        setEnabledInModalContext(true);
    }

    @NotNull
    @Override
    protected CodeInsightActionHandler getHandler() {
        return new XHandGenerateHandler();
    }

    @Override
    protected boolean isValidForFile(@NotNull Project project, @NotNull Editor editor, @NotNull final PsiFile file) {
        final Language language = file.getLanguage();
        if (!LanguageSurrounders.INSTANCE.allForLanguage(language).isEmpty()) {
            return true;
        }
        final PsiFile baseFile = PsiUtilCore.getTemplateLanguageFile(file);
        if (baseFile != null && baseFile != file && !LanguageSurrounders.INSTANCE.allForLanguage(baseFile.getLanguage()).isEmpty()) {
            return true;
        }

        if (!TemplateManagerImpl.listApplicableTemplateWithInsertingDummyIdentifier(editor, file, true).isEmpty()) {
            return true;
        }

        return false;
    }
}
