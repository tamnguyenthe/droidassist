package usu.seal.salad.evaluation;

import com.intellij.codeInsight.highlighting.HighlightManager;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.colors.EditorColors;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.editor.highlighter.EditorHighlighterFactory;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.groovy.ast.tools.GeneralUtils;
import usu.seal.salad.cfg.SourceCodeCFGCreator;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.completion.HAPICompletionContributor;
import usu.seal.salad.util.TypeUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class HAPISequenceEvaluationDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonApply;
    private JButton buttonCancel;
    private JTabbedPane tabbedPane;
    private JPanel originalSequencePanel;
    private JPanel replaceSequencePanel;
    private JPanel addSequencePanel;
    private JPanel deleteSequencePanel;

    private AnActionEvent actionEvent;

    private EditorEx originalSequenceEditor;
    private EditorEx replaceSequenceEditor;
    private EditorEx addSequenceEditor;
    private EditorEx deleteSequenceEditor;

    private List<SMethodSignature> methodSequence;
    private SObjectNode evalObjectNode;
    private HAPIEvaluator hapiEvaluator;
    private PsiClass psiClass;

    private Pair<SMethodSignature, Integer> replaceCandidate;
    private Pair<SMethodSignature, Integer> addCandidate;
    private int deleteCandidate;

    public HAPISequenceEvaluationDialog(AnActionEvent actionEvent) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonApply);

        //my code
        this.actionEvent = actionEvent;
        setTitle("Analyze API Call Sequence");

        buttonApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onApply();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        init();
    }

    private void onApply() {
        new WriteCommandAction.Simple(psiClass.getProject(), psiClass.getContainingFile()) {
            @Override
            protected void run() throws Throwable {
                applyAction();
            }
        }.execute();
        dispose();
    }

    private void applyAction() {
        switch (tabbedPane.getSelectedIndex()) {
            case 0:
                performReplace();
                break;
            case 1:
                performAdd();
                break;
            case 2:
                performDelete();
                break;
            default:
                break;
        }
    }

    private void performReplace() {
        if (replaceCandidate.getRight() == -1) return;
        PsiCall psiMethodCallToReplace = methodSequence.get(replaceCandidate.getRight()).getPsiCall();
        String replaceString = convertMethodSignatureToCode2(replaceCandidate.getLeft());
        PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(psiClass.getProject());
        PsiExpression expression = elementFactory.createExpressionFromText(replaceString, psiClass);
        psiMethodCallToReplace.replace(expression);
    }

    private void performAdd() {
        if (addCandidate.getRight() < methodSequence.size()) {
            PsiCall previousPsiCall = methodSequence.get(addCandidate.getRight()).getPsiCall();
            String replaceString = convertMethodSignatureToCode2(addCandidate.getLeft()) + ";";
            PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(psiClass.getProject());
            PsiStatement statementFromText = elementFactory.createStatementFromText(replaceString, psiClass);
            PsiStatement psiStatement = PsiTreeUtil.getParentOfType(previousPsiCall, PsiStatement.class);
            PsiElement parent = psiStatement.getParent();
            parent.addBefore(statementFromText, psiStatement);
        } else if (addCandidate.getRight() == methodSequence.size()) {
            PsiCall previousPsiCall = methodSequence.get(addCandidate.getRight()-1).getPsiCall();
            String replaceString = convertMethodSignatureToCode2(addCandidate.getLeft())+ ";";
            PsiElementFactory elementFactory = JavaPsiFacade.getElementFactory(psiClass.getProject());
            PsiStatement statementFromText = elementFactory.createStatementFromText(replaceString, psiClass) ;
            PsiStatement psiStatement = PsiTreeUtil.getParentOfType(previousPsiCall, PsiStatement.class);
            PsiElement parent = psiStatement.getParent();
            parent.addAfter(statementFromText, psiStatement);
        }
    }

    private void performDelete() {
        PsiCall previousPsiCall = methodSequence.get(deleteCandidate).getPsiCall();
        previousPsiCall.delete();
    }

    private void onCancel() {
        dispose();
    }

    void init() {
        int width = 550;
        int height = 250;

        PsiFile psiFile = actionEvent.getData(LangDataKeys.PSI_FILE);
        Editor editor = actionEvent.getData(PlatformDataKeys.EDITOR);
        int offset = editor.getCaretModel().getOffset();
        PsiIdentifier selectedIdentifier = (PsiIdentifier) psiFile.findElementAt(offset);
        PsiElement psiMethod = PsiTreeUtil.getParentOfType(selectedIdentifier, PsiMethod.class);
        SourceCodeCFGCreator sourceCodeCFGCreator = new SourceCodeCFGCreator((PsiMethod)psiMethod, null);
        sourceCodeCFGCreator.getCfgParameters().setVarToEval(selectedIdentifier);
        sourceCodeCFGCreator.buildCFG();
        methodSequence = sourceCodeCFGCreator.getMethodSequenceForEvaluation();
        evalObjectNode = sourceCodeCFGCreator.getCfgParameters().getEvalObjectNode();
        hapiEvaluator = new HAPIEvaluator(actionEvent.getProject(), evalObjectNode, HAPICompletionContributor.getPatternManager());
        psiClass = PsiTreeUtil.getParentOfType(selectedIdentifier, PsiClass.class);

        StringBuilder stringBuilder = new StringBuilder();
        for (SMethodSignature methodSignature : methodSequence) {
            stringBuilder.append(methodSignature.getPsiCall().getText());
            stringBuilder.append(";\n");
        }

        Document document = EditorFactory.getInstance().createDocument(stringBuilder.toString());
        originalSequenceEditor = (EditorEx)EditorFactory.getInstance().createEditor(document);
        originalSequenceEditor.getSettings().setLineMarkerAreaShown(false);
        originalSequencePanel.setLayout(new BorderLayout());
        originalSequencePanel.add(originalSequenceEditor.getComponent(), BorderLayout.CENTER);
        originalSequencePanel.setMaximumSize(new Dimension(width/2, height));
        originalSequencePanel.setMinimumSize(new Dimension(width/2, height));

        tabbedPane.setMaximumSize(new Dimension(width/2, height));
        tabbedPane.setMinimumSize(new Dimension(width/2, height));

        //replace
        replaceCandidate = hapiEvaluator.findReplaceCandidate(methodSequence);
        initReplace();

        //delete
        addCandidate = hapiEvaluator.findAddCandidate(methodSequence);
        initAdd();

        //add
        deleteCandidate = hapiEvaluator.findDeleteCandidate(methodSequence);
        initDelete();
    }

    private void initReplace() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < replaceCandidate.getRight(); index++) {
            stringBuilder.append(methodSequence.get(index).getPsiCall().getText());
            stringBuilder.append(";\n");
        }
        if (replaceCandidate.getRight() != -1) {
            stringBuilder.append(convertMethodSignatureToCode(replaceCandidate.getLeft()));
            stringBuilder.append(";\n");
        }
        for (int index = replaceCandidate.getRight()+1; index < methodSequence.size(); index++) {
            stringBuilder.append(methodSequence.get(index).getPsiCall().getText());
            stringBuilder.append(";\n");
        }
        Document document = EditorFactory.getInstance().createDocument(stringBuilder.toString());
        replaceSequenceEditor = (EditorEx)EditorFactory.getInstance().createEditor(document);
        replaceSequenceEditor.getSettings().setLineMarkerAreaShown(false);
        replaceSequencePanel.setLayout(new BorderLayout());
        replaceSequencePanel.add(replaceSequenceEditor.getComponent(), BorderLayout.CENTER);
    }

    private void initAdd() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < addCandidate.getRight(); index++) {
            stringBuilder.append(methodSequence.get(index).getPsiCall().getText());
            stringBuilder.append(";\n");
        }
        stringBuilder.append(convertMethodSignatureToCode(addCandidate.getLeft()));
        stringBuilder.append(";\n");

        for (int index = addCandidate.getRight(); index < methodSequence.size(); index++) {
            stringBuilder.append(methodSequence.get(index).getPsiCall().getText());
            stringBuilder.append(";\n");
        }
        Document document = EditorFactory.getInstance().createDocument(stringBuilder.toString());
        addSequenceEditor = (EditorEx)EditorFactory.getInstance().createEditor(document);
        addSequenceEditor.getSettings().setLineMarkerAreaShown(false);
        addSequencePanel.setLayout(new BorderLayout());
        addSequencePanel.add(addSequenceEditor.getComponent(), BorderLayout.CENTER);
    }

    private void initDelete() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < methodSequence.size(); index++) {
            if (index != deleteCandidate) {
                stringBuilder.append(methodSequence.get(index).getPsiCall().getText());
                stringBuilder.append(";\n");
            }
        }
        Document document = EditorFactory.getInstance().createDocument(stringBuilder.toString());
        deleteSequenceEditor = (EditorEx)EditorFactory.getInstance().createEditor(document);
        deleteSequenceEditor.getSettings().setLineMarkerAreaShown(false);
        deleteSequencePanel.setLayout(new BorderLayout());
        deleteSequencePanel.add(deleteSequenceEditor.getComponent(), BorderLayout.CENTER);
    }

    private String convertMethodSignatureToCode(SMethodSignature methodSignature) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(evalObjectNode.getIdentifier() + ".");
        stringBuilder.append(methodSignature.getName());
        stringBuilder.append("(");
        String[] parameters = methodSignature.getParameters();
        if (parameters.length > 0) {
            if (!TypeUtil.isJavaClass(parameters[0])) {
                stringBuilder.append(parameters[0]);
                stringBuilder.append(" ");
                stringBuilder.append(parameters[0].charAt(0));
            } else {
                String className = TypeUtil.getClassNameFromQualifiedName(parameters[0]);
                stringBuilder.append(className);
                stringBuilder.append(" ");
                stringBuilder.append(WordUtils.uncapitalize(className));
            }
            for (int j = 1; j < parameters.length; j++) {
                stringBuilder.append(", ");
                if (!TypeUtil.isJavaClass(parameters[j])) {
                    stringBuilder.append(parameters[j]);
                    stringBuilder.append(" ");
                    stringBuilder.append(parameters[j].charAt(0));
                } else {
                    String className = TypeUtil.getClassNameFromQualifiedName(parameters[j]);
                    stringBuilder.append(className);
                    stringBuilder.append(" ");
                    stringBuilder.append(WordUtils.uncapitalize(className));
                }
            }
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }


    private String convertMethodSignatureToCode2(SMethodSignature methodSignature) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(evalObjectNode.getIdentifier() + ".");
        stringBuilder.append(methodSignature.getName());
        stringBuilder.append("(");
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
