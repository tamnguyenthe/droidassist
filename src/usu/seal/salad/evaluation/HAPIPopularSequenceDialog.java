package usu.seal.salad.evaluation;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import usu.seal.salad.cfg.SourceCodeCFGCreator;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.completion.HAPICompletionContributor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class HAPIPopularSequenceDialog extends JDialog {
    private JPanel contentPane;
    private JPanel originalSequencePanel;
    private JPanel commentPanel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel commentLabel;

    private AnActionEvent actionEvent;
    private List<SMethodSignature> methodSequence;
    private SObjectNode evalObjectNode;
    private HAPIEvaluator hapiEvaluator;
    private PsiClass psiClass;
    private EditorEx originalSequenceEditor;

    public HAPIPopularSequenceDialog(AnActionEvent actionEvent) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Analyze API Call Sequence");

        this.actionEvent = actionEvent;

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        init();
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    void init() {
        int width = 550;
        int height = 250;

        PsiFile psiFile = actionEvent.getData(LangDataKeys.PSI_FILE);
        Editor editor = actionEvent.getData(PlatformDataKeys.EDITOR);
        int offset = editor.getCaretModel().getOffset();
        PsiIdentifier selectedIdentifier = (PsiIdentifier) psiFile.findElementAt(offset);
        PsiElement psiMethod = PsiTreeUtil.getParentOfType(selectedIdentifier, PsiMethod.class);
        SourceCodeCFGCreator sourceCodeCFGCreator = new SourceCodeCFGCreator((PsiMethod)psiMethod, null);
        sourceCodeCFGCreator.getCfgParameters().setVarToEval(selectedIdentifier);
        sourceCodeCFGCreator.buildCFG();
        methodSequence = sourceCodeCFGCreator.getMethodSequenceForEvaluation();
        evalObjectNode = sourceCodeCFGCreator.getCfgParameters().getEvalObjectNode();
        hapiEvaluator = new HAPIEvaluator(actionEvent.getProject(), evalObjectNode, HAPICompletionContributor.getPatternManager());
        psiClass = PsiTreeUtil.getParentOfType(selectedIdentifier, PsiClass.class);

        StringBuilder stringBuilder = new StringBuilder();
        for (SMethodSignature methodSignature : methodSequence) {
            stringBuilder.append(methodSignature.getPsiCall().getText());
            stringBuilder.append(";\n");
        }

        Document document = EditorFactory.getInstance().createDocument(stringBuilder.toString());
        originalSequenceEditor = (EditorEx)EditorFactory.getInstance().createEditor(document);
        originalSequenceEditor.getSettings().setLineMarkerAreaShown(false);
        originalSequencePanel.setLayout(new BorderLayout());
        originalSequencePanel.add(originalSequenceEditor.getComponent(), BorderLayout.CENTER);
        originalSequencePanel.setMaximumSize(new Dimension(width / 2, height));
        originalSequencePanel.setMinimumSize(new Dimension(width / 2, height));

        commentPanel.setMaximumSize(new Dimension(width / 2, height));
        commentPanel.setMinimumSize(new Dimension(width / 2, height));
        commentLabel.setText("<html>This API call sequence is considered<br/>a popular usage of the object.</html>");
    }

}
