package usu.seal.salad.evaluation;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import org.apache.commons.lang3.tuple.Pair;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.cfg.node.SObjectNode;
import usu.seal.salad.pattern.HAPI;
import usu.seal.salad.pattern.PatternManager;
import usu.seal.salad.util.TypeUtil;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TamNT on 7/17/15.
 */
public class HAPIEvaluator {
    private SObjectNode evalObjectNode;
    private PatternManager patternManager;
    private HAPI model;
    public HAPIEvaluator(Project project, SObjectNode evalObjectNode, PatternManager patternManager) {
        this.evalObjectNode = evalObjectNode;
        this.patternManager = patternManager;
        int objectID = patternManager.getObjectBiMap().inverse().get(TypeUtil.convertClassStringToDalvik(evalObjectNode.getType()));
        this.model = patternManager.loadModelForObject(objectID);
    }

    boolean evalMethodSequence(List<SMethodSignature> methodSequence) {
        List<Integer> methodIDSequence = new ArrayList<>();
        for (SMethodSignature methodSignature : methodSequence) {
            int methodID = patternManager.getMethodNameBiMap().inverse().get(methodSignature.toDalvikString());
            methodIDSequence.add(methodID);
        }
        return model.evalMethodIDSequence(methodIDSequence);
    }

    public Pair<SMethodSignature, Integer> findReplaceCandidate(List<SMethodSignature> methodSequence) {
        List<Integer> methodIDSequence = new ArrayList<>();
        for (SMethodSignature methodSignature : methodSequence) {
            int methodID = patternManager.getMethodNameBiMap().inverse().get(methodSignature.toDalvikString());
            methodIDSequence.add(methodID);
        }
        Pair<Integer, Integer> replaceCandidate = model.findReplaceCandidate(methodIDSequence);
        String methodString = patternManager.getMethodNameBiMap().get(replaceCandidate.getLeft());
        if (methodString == null) return Pair.of(null, -1); //current sequence is the best sequence
        return Pair.of(TypeUtil.convertMethodDalvikString(methodString), replaceCandidate.getRight());
    }

    public Pair<SMethodSignature, Integer> findAddCandidate(List<SMethodSignature> methodSequence) {
        List<Integer> methodIDSequence = new ArrayList<>();
        for (SMethodSignature methodSignature : methodSequence) {
            int methodID = patternManager.getMethodNameBiMap().inverse().get(methodSignature.toDalvikString());
            methodIDSequence.add(methodID);
        }
        Pair<Integer, Integer> addCandidate = model.findAddCandidate(methodIDSequence);
        String methodString = patternManager.getMethodNameBiMap().get(addCandidate.getLeft());
        return Pair.of(TypeUtil.convertMethodDalvikString(methodString), addCandidate.getRight());
    }

    public int findDeleteCandidate(List<SMethodSignature> methodSequence) {
        List<Integer> methodIDSequence = new ArrayList<>();
        for (SMethodSignature methodSignature : methodSequence) {
            int methodID = patternManager.getMethodNameBiMap().inverse().get(methodSignature.toDalvikString());
            methodIDSequence.add(methodID);
        }
        return model.findDeletePosition(methodIDSequence);
    }

    public SObjectNode getEvalObjectNode() {
        return evalObjectNode;
    }

    public PatternManager getPatternManager() {
        return patternManager;
    }

    public HAPI getModel() {
        return model;
    }
}
