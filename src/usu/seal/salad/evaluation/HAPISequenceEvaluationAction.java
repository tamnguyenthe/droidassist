package usu.seal.salad.evaluation;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.PathUtil;
import usu.seal.salad.cfg.SourceCodeCFGCreator;
import usu.seal.salad.cfg.node.SMethodSignature;
import usu.seal.salad.completion.HAPICompletionContributor;
import usu.seal.salad.pattern.PatternManager;

import java.io.File;
import java.util.List;

/**
 * Created by TamNT on 7/10/15.
 */
public class HAPISequenceEvaluationAction extends AnAction {
    public void actionPerformed(AnActionEvent e) {
        final Project project = e.getData(PlatformDataKeys.PROJECT);
        PsiFile psiFile = e.getData(LangDataKeys.PSI_FILE);
        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        int offset = editor.getCaretModel().getOffset();

        PsiIdentifier selectedIdentifier = null;
        try {
            selectedIdentifier = (PsiIdentifier) psiFile.findElementAt(offset);
        } catch (Exception exception) {
            Messages.showMessageDialog(project, "Please move cursor to a variable", "Information", Messages.getInformationIcon());
            return;
        }
        PsiElement psiMethod = PsiTreeUtil.getParentOfType(selectedIdentifier, PsiMethod.class);
        SourceCodeCFGCreator sourceCodeCFGCreator = new SourceCodeCFGCreator((PsiMethod)psiMethod, null);
        sourceCodeCFGCreator.getCfgParameters().setVarToEval(selectedIdentifier);
        sourceCodeCFGCreator.buildCFG();
        StringBuilder stringBuilder = new StringBuilder();
        List<SMethodSignature> methodSequence = sourceCodeCFGCreator.getMethodSequenceForEvaluation();
        HAPIEvaluator hapiEvaluator = new HAPIEvaluator(project, sourceCodeCFGCreator.getCfgParameters().getEvalObjectNode(), HAPICompletionContributor.getPatternManager());
        if (!hapiEvaluator.evalMethodSequence(methodSequence)) {
            HAPISequenceEvaluationDialog hapiSequenceEvaluationDialog = new HAPISequenceEvaluationDialog(e);
            hapiSequenceEvaluationDialog.setBounds(300, 200, 600, 350);
            hapiSequenceEvaluationDialog.setVisible(true);
        } else {
            HAPIPopularSequenceDialog hapiSequenceEvaluationDialog = new HAPIPopularSequenceDialog(e);
            hapiSequenceEvaluationDialog.setBounds(300, 200, 600, 350);
            hapiSequenceEvaluationDialog.setVisible(true);
        }
    }
}
