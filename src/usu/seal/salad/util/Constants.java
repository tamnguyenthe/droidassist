package usu.seal.salad.util;


/**
 * Created by TamNT on 7/7/15.
 */
public class Constants {
    public static final String INIT = "<init>";
    public static final String LOAD = "<LOAD>";
    public static final int LOAD_ID = -1;
    public static final char COMMA_CHAR = ',';
    public static final char QUOTE_CHAR = '\"';

    public static final String OBJECTS_FILE = "/HAPI/Objects.csv";
    public static final String METHOD_NAMES_FILE = "/HAPI/MethodNames.csv";
    public static final String MULTIPLE_OBJECTS_FILE = "/HAPI/MultipleObjects.csv";
    public static final String SINGLE_OBJECT_USAGE_DIR = "/HAPI/Single";
    public static final String MULTIPLE_OBJECT_USAGE_DIR = "/HAPI/Set"; //use later
    public static final double EPSILON = 0.00000000001;
}
