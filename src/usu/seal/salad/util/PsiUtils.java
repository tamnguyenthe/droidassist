package usu.seal.salad.util;

import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;

import javax.lang.model.type.ArrayType;

/**
 * Created by TamNT on 7/16/15.
 */
public class PsiUtils {

    public static boolean isStaticMethod(@NotNull PsiMethod psiMethod) {
        return psiMethod.getModifierList().hasModifierProperty("static");
    }

    public static boolean isStaticField(@NotNull PsiField psiField) {
        return psiField.getModifierList().hasModifierProperty("static");
    }

    public static boolean isObjectType(@NotNull PsiType psiType) {
        if (psiType instanceof PsiPrimitiveType) return false;
        if (psiType instanceof PsiArrayType) return false;
        return true;
    }


}
