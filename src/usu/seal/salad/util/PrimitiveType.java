package usu.seal.salad.util;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by TamNT on 6/29/15.
 */
public class PrimitiveType {
    public static final String VOID 	= "V";
    public static final String BOOLEAN	= "Z";
    public static final String BYTE 	= "B";
    public static final String SHORT 	= "S";
    public static final String CHAR 	= "C";
    public static final String INT 		= "I";
    public static final String LONG 	= "J";
    public static final String FLOAT	= "F";
    public static final String DOUBLE	= "D";
    public static final String STRING	= "T";
    private static BiMap<String, String> primitiveMap;

    static {
        primitiveMap = HashBiMap.create();
        primitiveMap.put("void", VOID);
        primitiveMap.put("boolean", BOOLEAN);
        primitiveMap.put("byte", BYTE);
        primitiveMap.put("short", SHORT);
        primitiveMap.put("char", CHAR);
        primitiveMap.put("int", INT);
        primitiveMap.put("long", LONG);
        primitiveMap.put("float", FLOAT);
        primitiveMap.put("double", DOUBLE);
    }

    public static BiMap<String, String> getPrimitiveMap() {
        return primitiveMap;
    }
}
