# DroidAssist

## Introduction
**DroidAssist** a recommending tool for API usages. When a developer is writing code, DroidAssist can analyze the code being written and recommend (and fill-in on request) the next or missing API method calls. To help the developer makes more effective choices, those calls are ranked based on their likelihoods of appearance in the existing code context. DroidAssist can also detect suspicious API usages in existing code (i.e. ones rarely/unlikely to be used) and repair them with more probable usages. More details will be discussed in the next sections.

**DroidAssist** is released as a plugin of Android Studio, the standard IDE for Android apps development. After installation, it is incorporated with Android Studio and users can invoke it directly from the current editing view (for method call recommendation) or via the menu (for method sequence validation).

The corresponding paper of this tool is published at 2015 30th IEEE/ACM International Conference on Automated Software Engineering (ASE): https://ieeexplore.ieee.org/document/7372069

## Installation
1. Checkout the source code in this project
2. Open the project in Intellij IDEA with Plugin Development enabled
3. Build the plugin file (jar file) to use the plugin.
4. Install the plugin in Intellij IDEA or Android Studio.

## Usage
### Overall User Interface

![Figure 0](https://bitbucket.org/tamnguyenthe/droidassist/raw/0d28077e0a8e18192ffcb480b656a8e2c56919e5/Resources/figures/ScreenShot1.png)

### Recommending Next Method Call

Assume that the developer wants to write code for a database transaction. She has created a database query that return a Cursor object and made a call to begin the transaction.
However, she forgets how to use the returned Cursor object properly. She invokes the built-in code completion engine, but it just lists all methods that can be called on the Cursor, thus
does not help her to make an appropriate selection. Now, with DroidAssist, the developer will have better recommendations. The figure above shows a screenshot of Android Studio with DroidAssist invoked (via the keystroke Ctrl + Shift + Space)
for the current editing code. 

As seen, DroidAssist displays a ranked list of methods that can be called for the Cursor object. Each method has a score represents the probability of how
likely it might be called in the given current context of the object and other interacting objects. In this example, method moveToFirst has the highest score of 30.52%. If the developer chooses it, it will be filled in the current position in the editor.

### Analyzing Method Sequence

DroidAssist can analyze a given method sequence in existing code report. If it is a suspicious API usages (i.e. is
rarely used or unlikely to be used), DroidAssist can offer fixes with more probable method sequences.

DroidAssist then will analyze the API usage for that object. Figure bellow shows the analyzed result. The left of the dialog shows the original method sequence. The right of the dialog shows
the suggestions for repair. If DroidAssist detects a suspicious method sequence, it will suggest three actions: replace, add, or delete a method call, to make the usage more probable. In the example, DroidAssist detects that calling setAudioChannels at the beginning might not be a proper usage. It recommends to replace that method by setAudioSource. If the user choose option Replace and press Apply, DroidAssist will repair the API call sequence in the code editor by replacing setAudioChannels with setAudioSource.

![Figure 1](https://bitbucket.org/tamnguyenthe/droidassist/raw/c38d0fcd04018f7413edba7406695901340f6bc2/Resources/figures/analyze_sequence.png)

